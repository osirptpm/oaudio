<?php
/**
 * Created by IntelliJ IDEA.
 * User: Osirp
 * Date: 2017-01-20
 * Time: 오후 6:24
 */

namespace oMusic;

/** 해당 네임스페이스 사용*/
use oMusic\application\Controllers\Controller;

class index
{
    function __construct()
    {
        new Controller();
    }
}

/** 네임스페이스와 클래스명으로 php 파일 자동 require_once
 * (연결된 다른 모든 php 파일은 이 코드 작성 필요 없음)
 */
spl_autoload_register(function ($class) {
    require_once '..\\' . $class . '.php';
});

new index();