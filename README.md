# 개인 음감 사이트
## using IIS, MYSQL, PHP


1. IIS 사이트 루트 폴더를  프로젝트 폴더로 설정 (oaudio -> oMusic 으로 프로젝트 폴더 이름 변경 필요)
2. serverSetting.txt, sqlSetting.txt를 보고 설정
3. Wsocket.php php로 실행
4. 프로젝트 검색 fixme - 알맞게 변경

Wsocket.php 는 ipv4 주소를 사용하기 때문에 접속 웹 주소도 해당 주소와 같아야함. (localhost X)

![playNupload-screen](playNupload-screen.jpg)