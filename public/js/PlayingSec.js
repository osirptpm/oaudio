/**
 * Created by Osirp on 2017-02-04.
 */
// 이 페이지에서 사용될 함수들
function changePlayingSec() {
    state.playlist.currentPlaylist = state.playlist.totalPlaylist; // 재생목록 설정
    AC.setTracks = state.playlist.currentPlaylist;
    if (state.verified) { // 로그인시 실행할 코드
        // 함수 선언
        AC.play();

        // 이벤트 등록

        // 플레이 섹션 내려옴
        hashStateControl.add("playingSec");

    } else {
        // 플레이 섹션 올라감
        hashStateControl.remove("playingSec");

        AC.pause();

        // 이벤트 제거

        // 앨범아트 제거
        ja_playingDiv.setAttribute("data-src", state.detail.randomArt);
        ja_albumArt.style.backgroundImage = "url('')";
    }

    createPlaylist(AC.getTracks);
}

// 재생목록 생성
function createPlaylist(playlist) {
    let songs = "";
    if (state.verified && state.status != "guest-mode") {
        for (let i = 0; i < playlist.length; i++) {
            let title = (playlist[i].title != null) ? playlist[i].title : playlist[i].filename;
            songs += "<div class='songDiv'><span>" + title + "</span></div>";
        }
    }
    ja_playlistSec.innerHTML = songs;
}

// 스크롤바 없애기
function hideScrollBar(el) {
    let clientWidth = el.clientWidth,
        offsetWidth = el.offsetWidth;
    if (clientWidth < offsetWidth) {
        el.style.width = offsetWidth + offsetWidth - clientWidth + "px";
    }
}

// 이전, 다음 버튼 그리기
function DrawController() {
    let ja_previousCanvas_btn = document.getElementById("previousCanvas_btn");
    let pCtx = ja_previousCanvas_btn.getContext("2d");
    pCtx.moveTo(70, 5);
    pCtx.lineTo(5, 70);
    pCtx.lineTo(70, 140);
    pCtx.lineCap = "round";
    pCtx.lineWidth = 2;
    pCtx.strokeStyle = "rgba(255, 255, 255, 1)";
    pCtx.shadowColor = "rgba(0, 0, 0, .8)";
    pCtx.shadowOffsetY = 2;
    pCtx.shadowBlur = 5;
    pCtx.stroke();
    pCtx.shadowColor = "rgba(0, 0, 0, .5)";
    pCtx.shadowBlur = 10;
    pCtx.stroke();

    let ja_nextCanvas_btn = document.getElementById("nextCanvas_btn");
    let nCtx = ja_nextCanvas_btn.getContext("2d");
    nCtx.moveTo(5, 5);
    nCtx.lineTo(70, 70);
    nCtx.lineTo(5, 140);
    nCtx.lineCap = "round";
    nCtx.lineWidth = 2;
    nCtx.strokeStyle = "rgba(255, 255, 255, 1)";
    nCtx.shadowColor = "rgba(0, 0, 0, .8)";
    nCtx.shadowOffsetY = 2;
    nCtx.shadowBlur = 5;
    nCtx.stroke();
    nCtx.shadowColor = "rgba(0, 0, 0, .5)";
    nCtx.shadowBlur = 10;
    nCtx.stroke();
}

// 재생 프로그레스 그리기
function DrawBarProgress(ja_canvas) {
    this.cnvs = ja_canvas;
    this.ctx = ja_canvas.getContext("2d");
    this.percentage = 0;
}
DrawBarProgress.prototype.setPercentage = function (value) {
    this.percentage = (this.cnvs.width -20) * value / 100;
    // 캔버스 지우기
    this.ctx.clearRect(0, 0, this.cnvs.width, this.cnvs.height);
    this.ctx.beginPath();
    this.ctx.moveTo(10, 10);
    this.ctx.lineTo(this.cnvs.width - 10, 10);
    this.ctx.lineCap = "round";
    this.ctx.lineWidth = 3;
    this.ctx.strokeStyle = "rgba(255, 255, 255, .2)";
    this.ctx.shadowColor = "rgba(0, 0, 0, 1)";
    this.ctx.shadowOffsetY = 1;
    this.ctx.shadowBlur = 5;
    this.ctx.stroke();
    this.ctx.beginPath();
    this.ctx.moveTo(10, 10);
    this.ctx.lineTo(this.percentage + 10, 10);
    this.ctx.lineCap = "round";
    this.ctx.lineWidth = 3;
    this.ctx.strokeStyle = "rgba(255, 255, 255, 1)";
    this.ctx.shadowColor = "rgba(0, 0, 0, .7)";
    this.ctx.shadowOffsetY = 1;
    this.ctx.shadowBlur = 5;
    this.ctx.stroke();
};


// state 객체를 받기 전에 해도 되는 작업
// 엘리먼트 변수 선언
let ja_oMusicArt = document.getElementById("oMusicArt"),
    ja_playingDiv = document.getElementById("playingDiv"),
    ja_albumArt = document.getElementById("albumArt"),
    ja_playlistSec = document.getElementById("playlistSec"),
    ja_currentSongInfo_name = document.getElementById("currentSongInfo_name"),
    ja_currentSongInfo_artist = document.getElementById("currentSongInfo_artist"),
    ja_progressCanvas = document.getElementById("progressCanvas"),
    ja_previousCanvas_btn = document.getElementById("previousCanvas_btn"),
    ja_nextCanvas_btn = document.getElementById("nextCanvas_btn");

// 기타 변수 선언

// 사용자 정의 함수 선언
let AC = new AudioController();
let progressBar = new DrawBarProgress(ja_progressCanvas);

// 페이지 로드시 실행 코드
VCBHPLoad.setKeyNFunc({
    playingSec: {
        "on": function () { // 해시가 있을때
            ja_oMusicArt.classList.add("playingSec");
        },
        "off": function () { // 해시가 없을때
            ja_oMusicArt.classList.remove("playingSec");
        }
    }
});
VCBHP.checkAllFunc();
DrawController();
progressBar.setPercentage(60);
hideScrollBar(ja_playlistSec);


// 이벤트 등록
AC.addEventListener("timeupdate", function () {
    progressBar.setPercentage(100 * AC.currentTime / AC.duration);
});
AC.addEventListener("ended", function () {AC.next();});
AC.addEventListener("loadedmetadata", function () {
    ja_playingDiv.setAttribute("data-src", AC.getCurrentTrackInfo.albumArtSrc);
    ja_albumArt.style.backgroundImage = "url(\"" + AC.getCurrentTrackInfo.albumArtSrc + "\")";
    ja_playingDiv.blurEffect();

    ja_currentSongInfo_name.innerHTML = AC.getCurrentTrackInfo.title || AC.getCurrentTrackInfo.filename;
    ja_currentSongInfo_artist.innerHTML = AC.getCurrentTrackInfo.artist;
});
ja_albumArt.addEventListener("click", function () {AC.PP();});
ja_previousCanvas_btn.addEventListener("click", function () {AC.previous();});
ja_nextCanvas_btn.addEventListener("click", function () {AC.next();});