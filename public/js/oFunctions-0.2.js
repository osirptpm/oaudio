/**
    * Created by Osirp on 2017-02-02.
    */

/**
 * 이미지 블러 처리 함수
 * 프로토타입 사용
 * <div id="playingSec" data-src="/public/images/Cover.jpg"></div>
 * var ja_playingSec = document.getElementById("playingSec");
 * ja_playingSec.blurEffect();
 * ja_playingSec.blurEffect("/public/images/Cover.jpg");
 * ja_playingSec.blurEffect("/public/images/Cover.jpg", 5);
 * ja_playingSec.blurEffect("/public/images/Cover.jpg", function (data) {
 *      console.log(data);
 * });
 * ja_playingSec.blurEffect("/public/images/Cover.jpg", 5, function (data) {
 *      console.log(data);
 * });
 */
/**
 * @param {string} ImageSrc
 * @param {number, optional} [Radius = 5]
 * @param {function, optional} CallBackURL
 */
HTMLElement.prototype.blurEffect = function (ImageSrc, Radius, CallBackURL) {
    // 인수 처리
    var imgSrc, radius, returnSrc;
    for (var i = 0; i < 3; i++) {
        if (typeof arguments[i] === "string") imgSrc = arguments[i];
        else if (typeof arguments[i] === "number") radius = arguments[i];
        else if (typeof arguments[i] === "function") returnSrc = arguments[i];
    }
    imgSrc = imgSrc || this.getAttribute("src") || this.getAttribute("data-src");
    if (imgSrc == null || imgSrc == undefined || imgSrc == "") return false;
    if (radius === 0) {
        __apply(this, imgSrc);
        return false;
    }
    radius = radius || 7; // 0 || 10 ==> 10
    returnSrc = returnSrc || function () {};


    var rW, rH;
    var el = this;
    var img = new Image();
    img.src = imgSrc;

    img.onload = function () {
        var nW = img.naturalWidth;
        var nH = img.naturalHeight;
        var rWidth = Math.round(radius * -50 + 600);
        rW = (rWidth <= 100) ? 100 : rWidth;
        rH = Math.round(rW * nH / nW); // 소수점 나오면 블러픽셀 계산 안된다

        var canvas = document.createElement("canvas");
        var ctx = canvas.getContext("2d");
        canvas.width = rW;
        canvas.height = rH;

        ctx.drawImage(img, 0, 0, rW, rH);
        var imageData = ctx.getImageData(0, 0, rW, rH);
        for (var i = 0; i < radius; i++)
        {
            __blur(imageData);
        }
        ctx.putImageData(imageData, 0, 0);
        __apply(el, canvas.toDataURL());
    };
    /**
     * i % bW >= 4 // 왼쪽
     * i % bW < bW - 4 // 오른쪽
     * i >= bW // 위쪽
     * i < bW * bH - bW // 아래쪽
     */
    function __blur(imageData) {
        var bW = rW * 4,
            bH = rH;
        var iData = imageData.data;
        var iDataLen = iData.length;
        for (var i = 0; i < iDataLen - 4; i+=4) {
            var rSum = 0, gSum = 0, bSum = 0, aSum = 0;
            var bitArr = [];
            // 중복 계산 피하기
            var UL = i - bW - 4,
                UM = i - bW,
                UR = i - bW + 4,
                ML = i - 4,
                MM = i,
                MR = i + 4,
                BL = i + bW - 4,
                BM = i + bW,
                BR = i + bW + 4;

            // 중복 계산 피하기
            var ibW = i % bW,
                bw4 = bW - 4,
                bWbHbW =  bW * bH - bW;

            if (ibW >= 4 && // 왼쪽
                ibW < bw4 && // 오른쪽
                i >= bW && // 위쪽
                i < bWbHbW) // 아래쪽
            { // 안쪽
                bitArr = [
                    UL, UM, UR,
                    ML, MM, MR,
                    BL, BM, BR
                ];
            }
            // 바깥쪽
            else if (ibW < 4 && i < bW) { // 왼쪽 위
                bitArr = [
                    MM, MR,
                    BM, BR
                ];
            } else if (ibW >= bw4 && i < bW) { // 오른쪽 위
                bitArr = [
                    ML, MM,
                    BL, BM
                ];
            } else if (ibW < 4 && i >= bWbHbW) { // 왼쪽 아래
                bitArr = [
                    UM, UR,
                    MM, MR,
                ];
            } else if (ibW >= bw4 && i >= bWbHbW) { // 오른쪽 아래
                bitArr = [
                    UL, UM,
                    ML, MM
                ];
            } else if (ibW < 4) { // 왼쪽
                bitArr = [
                    UM, UR,
                    MM, MR,
                    BM, BR
                ];
            } else if (ibW >= bw4) { // 오른쪽
                bitArr = [
                    UL, UM,
                    ML, MM,
                    BL, BM
                ];
            } else if (i < bW) { // 위쪽
                bitArr = [
                    ML, MM, MR,
                    BL, BM, BR
                ];
            } else if (i >= bWbHbW) { // 아래쪽
                bitArr = [
                    UL, UM, UR,
                    ML, MM, MR,
                ];
            }

            var bitArrLen = bitArr.length;
            for (var j= 0; j < bitArrLen; j++) {
                rSum += iData[bitArr[j]];
                gSum += iData[bitArr[j] + 1];
                bSum += iData[bitArr[j] + 2];
                aSum += iData[bitArr[j] + 3];
            }
            iData[i] = rSum / bitArrLen;
            iData[i + 1] = gSum / bitArrLen;
            iData[i + 2] = bSum / bitArrLen;
            iData[i + 3] = aSum / bitArrLen;
        }
    }
    function __apply(el, src) {
        if (el.src === undefined) {
            el.style.backgroundImage = "url('"+ src +"')";
        } else {
            el.src = src;
        }
        returnSrc(src);
    }

};
/**
 * 반쪽 짜리 자동완성 함수
 * 목록 배열로 주면 키워드에 따라 정렬 리턴
 * var ja_signInput_email = document.getElementById("signInput_email");
 * var ja_autoCompleteDiv = document.getElementById("autoCompleteDiv");
 * var emailAddress = ["daum.net", "naete.com", "nacte.com", "google.com", "hotmail.com", "aadum.net", "naver.com", "hanmail.net"];
 * var IAC = new InputAutoComplete(ja_signInput_email, emailAddress);
 * IAC.setContainer = ja_autoCompleteDiv; // 옵션 출력 엘리먼트
 * IAC.setCustomChildHTML = "<div>@value</div>"; // 옵션 출력될 자식 엘리먼트
 * IAC.getResult("검색키워드"); // 이벤트 등록 안했을때
 * IAC.setEventListener(true, "keyup"); // ja_signInput_email.value 값으로 자동
 */
function InputAutoComplete(inputEl, dataArr) {
    var resultArr = [];
    var input = inputEl;
    var value;
    var container, visible = false;
    var frontHTML = "<div class='__autoCompleteChildDiv'><span class='__autoCompleteChildSpan'>",
        endHTML = "</span></div>";
    var type = "keyup";

    this.__defineSetter__("setDataArr",function (arr) {
        dataArr = arr;
    });
    this.__defineSetter__("setContainer",function (el) {
        container = el;
        visible = true;
    });
    this.__defineSetter__("setCustomChildHTML",function (str){
        var tmp = str.split("@value");
        frontHTML = tmp[0];
        endHTML = tmp[1];
    });
    this.__defineSetter__("setVisible",function (bool) {
        visible = bool;
    });
    this.setEventListener = function (bool, eventType) {
        __setEventListener(bool, eventType);
    };
    this.getResult = function (str) {
        return __getResult(str);
    };

    function __setEventListener(bool, eventType) {
        if (bool) {
            type = eventType || type;
            input.addEventListener(type, __autoComplete);
        } else {
            input.removeEventListener(type, __autoComplete);
        }
    }
    function __getResult(str) {
        value = str;
        __autoComplete();
        return resultArr
    }
    function __autoComplete() {
        var val = (value !== undefined) ? value : input.value;
        __searchArr(val);
        __sortArr(val);
        if (visible) __displayArr();
    }
    function __searchArr(val) {
        for (var i in dataArr) {
            if (resultArr.indexOf(dataArr[i]) === -1 && dataArr[i].indexOf(val) !== -1) {
                resultArr.push(dataArr[i]);
            }
        }
        for (var i = 0; i < resultArr.length; i++) {
            if (resultArr[i].indexOf(val) === -1) {
                resultArr.splice(i, 1);
                i--;
            }
        }
    }
    function __sortArr(val) {
        resultArr.sort();
        if (val.length !== 0) {
            var resultArrLen = resultArr.length - 1;
            for (var i = resultArrLen, j = resultArrLen; j >= 0; i--, j--) {
                if (resultArr[i].indexOf(val) === 0) {
                    resultArr.unshift(resultArr.splice(i, 1)[0]);
                    i++
                }
            }
        }
    }
    function __displayArr() {
        container.innerHTML = "";
        for (var i in resultArr) {
            container.innerHTML += frontHTML + resultArr[i] + endHTML;
        }
    }
}

/**
 * hash 배열,문자열함수로 hash 조작 함수
 * hashStateControl.add("값");
 */
window.hashStateControl = {
    bodyScrollTop: null,
    locationHash: null,
    hashArr: null,
    hashCheck: null,
    hasHash: function (key) {
        this.__propertyReset(key);
        if (this.hashCheck == -1) { // 없다면
            return false;
        } else { // 있다면
            return true;
        }
    },
    add: function (key) {
        this.__saveScrollTop();
        this.__propertyReset(key);
        if (this.hashCheck == -1) { // 없다면
            location.hash = this.hashArr.join("#") + "#"+ key;
        } else { // 있다면
            location.hash = this.hashArr.join("#");
        }
        this.__loadScrollTop();
        return location.hash;
    },
    remove: function (key) {
        this.__saveScrollTop();
        this.__propertyReset(key);
        if (this.hashCheck == -1) { // 없다면
            location.hash = this.hashArr.join("#");
        } else { // 있다면
            this.hashArr.splice(this.hashCheck, 1);
            location.hash = this.hashArr.join("#");
        }
        this.__loadScrollTop();
        return location.hash;
    },
    toggle: function (key) {
        this.__saveScrollTop();
        this.__propertyReset(key);
        if (this.hashCheck == -1) { // 없다면
            location.hash = this.hashArr.join("#") + "#"+ key; // 만들고
        } else { // 있다면
            this.hashArr.splice(this.hashCheck, 1); // 지우고
            location.hash = this.hashArr.join("#");
        }
        this.__loadScrollTop();
        return location.hash;
    },
    replace: function (key, value) {
        this.__saveScrollTop();
        this.__propertyReset(key);
        if (this.hashCheck == -1) { // 없다면
            return this.locationHash; // 아무것도 안함
        } else { // 있다면
            this.hashArr.splice(this.hashCheck, 1); // 지우고
            if (this.hashArr.indexOf(value) == -1) { // 바꾸려는 값이 없다면
                location.hash = this.hashArr.join("#") + "#"+ value; // 넣고
            } else { // 있다면
                location.hash = this.hashArr.join("#");
            }
        }
        this.__loadScrollTop();
        return location.hash;
    },
    __propertyReset: function __propertyReset(key) {
        this.locationHash = location.hash;
        this.hashArr = this.locationHash.split("#");
        this.hashCheck = this.hashArr.indexOf(key);
    },
    __saveScrollTop: function __saveScrollTop() {
        // 스크롤탑으로 가는거 방지
        this.bodyScrollTop = document.documentElement.scrollTop || document.body.scrollTop;
    },
    __loadScrollTop: function () {
        // 스크롤탑으로 가는거 방지
        document.body.scrollTop = this.bodyScrollTop;
        document.documentElement.scrollTop = this.bodyScrollTop;
    }
};


/*
 주소값(hash)에 따른 DOM 컨트롤 함수
    var VCBHP = new ViewControlByHashParam(), // 주소값에 따른 DOM 컨트롤 함수
    var VCBHPLoad = VCBHP.loadChainFunc(); // chainFunc 오브젝트 로드
    VCBHPLoad.setKeyNFunc("keyname",function,function)
    .setKeyNFunc("keyname",function,function); // 메소드 체인 가능
    VCBHPLoad.setKeyNFunc({
    "keyname": {
    "on": function, // 해시가 있을때
    "off": function // 해시가 없을때
    },
    "keyname": {
    "on": function,
    "off": function
    }
    }); // 오브젝트로 넘기기 가능
    VCBHP.onFunc("keyname"); // 설정된 on함수 실행
    VCBHP.offFunc("keyname"); // 설정된 off함수 실행
    VCBHP.checkAllFunc(); 주소값에 keyNFunc 오브젝트에 설정된 key 값이 있을때 onFunc함수 실행, 없을때 offFunc함수 실행
 */

function ViewControlByHashParam() {
    var keyNFuncOn = {},
        keyNFuncOff = {};
    var chainFunc = { // 메소드 체인을 위해 오브젝트로 구성
        setKeyNFunc : function (key, func1, func2) {
            if (typeof key == "object") {
                var obj = key,
                    keys = Object.keys(obj);
                for (var i in keys) {
                    key = keys[i]; // 해시값
                    func1 = obj[key].on;  // 해당 함수
                    func2 = obj[key].off;  // 해당 함수
                    keyNFuncOn[key] = (typeof func1 == "function") ? func1 : null;
                    keyNFuncOff[key] = (typeof func2 == "function") ? func2 : null;
                }
            } else {
                keyNFuncOn[key] = (typeof func1 == "function") ? func1 : null;
                keyNFuncOff[key] = (typeof func2 == "function") ? func2 : null;
            }
            return this; // 메소드 체인을 위해 this 리턴
        },
        onFunc : function (key) {
            keyNFuncOn[key]();
            return this; // 메소드 체인을 위해 this 리턴
        },
        offFunc : function (key) {
            keyNFuncOff[key]();
            return this;
        }
    };

    this.loadChainFunc = function () {
        return __loadChainFunc();
    };
    this.checkAllFunc = function () {
        __checkAllFunc();
    };

    function __loadChainFunc() {
        return chainFunc;
    }
    // 현재 주소값에 key 값이 있다면 해당 함수 실행
    function __checkAllFunc() {
        var locationHash = location.hash;
        //console.log(locationHash);
        for (var key in keyNFuncOn) {
            var patt = new RegExp("#"+key+"#|#"+key+"$", "g");
            if (patt.test(locationHash)) {
                if (typeof keyNFuncOn[key] == "function")
                    keyNFuncOn[key]();
            } else {
                if (typeof keyNFuncOff[key] == "function")
                    keyNFuncOff[key]();
            }
        }
        console.log("해시작동!");
    }
    window.addEventListener("hashchange", __checkAllFunc);

    /*this.__defineGetter__("getKeyNFuncOn", function () {
     return keyNFuncOn;
     });
     this.__defineGetter__("getKeyNFuncOff", function () {
     return keyNFuncOff;
     });*/

}

// 오디오 컨트롤러
class AudioController extends Audio {
    constructor () {
        super();
        this.currentTrack = 0;
        this.playing = false;
    }
    get getCurrentTrackInfo() {
        return this.currentTracks[this.currentTrack];
    }
    get getTracks() {
        return this.currentTracks;
    }
    set setTracks(tracks) {
        this.currentTracks = tracks;
        if (this.currentTracks != null && this.currentTracks.length != 0 && super.paused) this.src = this.currentTracks[this.currentTrack].src;
    }
    play() {
        if (super.paused) {
            super.play();
            this.playing = true;
        }
    }
    pause() {
        super.pause();
        this.playing = false;
    }
    PP() {
        if (super.paused) super.play();
        else super.pause();
    }
    previous() {
        if (this.currentTracks == null) return false;
        if (this.currentTrack > 0) {
            this.src = this.currentTracks[--this.currentTrack].src;
        } else {
            this.currentTrack = this.currentTracks.length - 1;
            this.src = this.currentTracks[this.currentTrack].src;
        }
        if (this.playing) super.play();
    }
    next() {
        if (this.currentTracks == null) return false;
        if (this.currentTracks.length > this.currentTrack + 1) {
            this.src = this.currentTracks[++this.currentTrack].src;
        } else {
            this.currentTrack = 0;
            this.src = this.currentTracks[this.currentTrack].src;
        }
        if (this.playing) super.play();
    }
}