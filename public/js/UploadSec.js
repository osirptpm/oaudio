/**
 * Created by Osirp on 2017-02-06.
 */

// 이 페이지에서 사용될 함수들 // 프로토타입 선언은 호이스팅 x
// 업로드 진행 상황 표시 함수
function DrawCircleProgress(ja_canvas) {
    this.cnvs = ja_canvas;
    this.ctx = ja_canvas.getContext("2d");
    this.percentage = 0;
    this.dgree = Math.PI / 180;
}
DrawCircleProgress.prototype.setPercentage = function (value) {
    this.percentage = 360 * value / 100;
    // 캔버스 지우기
    this.ctx.clearRect(0, 0, this.cnvs.width, this.cnvs.height);
    // 바탕 원 그리기
    this.ctx.beginPath();
    this.ctx.arc(35, 35, 20, 0, Math.PI * 2); // Outer circle
    this.ctx.fillStyle = "rgba(255, 255, 255, .7)";
    this.ctx.fill();
    this.ctx.lineWidth = 5;
    this.ctx.strokeStyle = "rgba(52, 73, 94, 1)";
    this.ctx.shadowColor = "rgba(0, 0, 0, .8)";
    this.ctx.shadowBlur = 5;
    this.ctx.stroke();
    this.ctx.shadowColor = "rgba(0, 0, 0, .5)";
    this.ctx.shadowBlur = 10;
    this.ctx.shadowOffsetX = 0;
    this.ctx.shadowOffsetY = 2;
    this.ctx.stroke();

    // 진행 원 그리기
    this.ctx.beginPath();
    //ctx.lineCap="round";
    this.ctx.arc(35, 35, 20, - this.dgree * 90, - this.dgree * 90 + (this.dgree * this.percentage)); // Outer circle
    this.ctx.lineWidth = 5;
    this.ctx.strokeStyle = "rgba(255, 255, 255, .9)";
    this.ctx.lineCap = 'round';
    this.ctx.shadowColor = "rgba(0, 0, 0, 0)";
    this.ctx.shadowBlur = 0;
    this.ctx.shadowOffsetX = 0;
    this.ctx.shadowOffsetY = 0;
    this.ctx.stroke();
};



// 엘리먼트 변수 선언
var ja_uploadForm = document.getElementById("uploadForm"),
    ja_upload_btn = document.getElementById("upload_btn"),
    ja_uploadCanvas = document.getElementById("uploadCanvas"),
    ja_fileInput = document.getElementById("fileInput");

// 기타 변수 선언

// 사용자 정의 함수 선언
var progressCircle = new DrawCircleProgress(ja_uploadCanvas); // 업로드 프로그레스

// 페이지 로드시 실행 코드
progressCircle.setPercentage(70);

// 이벤트 등록
ja_fileInput.onchange = function () {
    var formData = new FormData(ja_uploadForm);
    //formData.append("socketId", socketId); // 소켓 ID 같이 보냄
    $.ajax({
        beforeSend: function(jqXHR){
            jqXHR.setRequestHeader("X-Requested-FileUpload", "Upload");
        },
        xhr: function () {
            var xhr = new window.XMLHttpRequest();
            xhr.upload.addEventListener("progress", function (event) {
                if (event.lengthComputable) {
                    var percentComplete = event.loaded / event.total;
                    progressCircle.setPercentage(percentComplete * 100);
                }
            });
            /*xhr.addEventListener("progress", function (event) {
             // 다운로드 프로그레스
             if (event.lengthComputable) {
             var percentComplete = event.loaded / event.total;
             }
             });*/
            return xhr;
        },
        method: "post",
        data: formData,
        contentType: false,
        processData: false,
        success: function (result) {
            console.log(result);
        },
        error: function (jqXHR, textStatus, errorThrown) { // 서버단 에러
            console.log(textStatus + " : " + errorThrown);
        }
    });
};
ja_upload_btn.onclick = function () {
    ja_fileInput.click();
};


