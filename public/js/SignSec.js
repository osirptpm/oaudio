/**
 * Created by Osirp on 2017-02-03.
 */
// 이 페이지에서 사용될 함수들
function changeSignSec() {
    if (state.verified) {
        ja_signInSec.classList.add("verified");
        ja_signInput_email.readOnly = true;
        ja_signInput_email.value = state.email;
        ja_signInput_email.blur();
        if (state.status == "edit-mode") {
            ja_signInSec.classList.add("edit-mode");
            ja_signInput_passwd.readOnly = true;
            ja_signInput_passwd.value = "   ";
            ja_signInput_passwd.blur();
        }
        ja_signInSpan.innerHTML = "Sign Out :";
        ja_signModeSpan.innerHTML = state.status;
    } else {
        ja_signInSec.classList.remove("verified");
        ja_signInSec.classList.remove("edit-mode");
        ja_signInput_email.readOnly = false;
        ja_signInput_passwd.readOnly = false;
        ja_signInput_passwd.value = "";
        ja_signInSpan.innerHTML = "Sign In :";
        ja_signModeSpan.innerHTML = checkSignInMode();
    }
}

// 이메일 주소인지 확인
function checkEmailAddress(value) {
    return /^\w+@\w+(\.\w+){1,2}$/.exec(value);
}

// 로그인 시도하는 모드 판별
function checkSignInMode() {
    let result;
    if (ja_signInput_email.value.length == 0) result = "guest-mode";
    else {
        if (ja_signInput_passwd.value.length == 0) result = "listening-mode";
        else result = "edit-mode";
    }
    return result;
}

// 엘리먼트 변수 선언
let ja_moveContainer = document.getElementById("moveContainer"),
    ja_signInSec = document.getElementById("signInSec"),
    ja_signUpSecSpan = document.getElementById("signUpSecSpan"),
    ja_signInput_email = document.getElementById("signInput_email"),
    ja_signInput_passwd = document.getElementById("signInput_passwd"),
    ja_signInSpan = document.getElementById("signInSpan"),
    ja_signModeSpan = document.getElementById("signModeSpan"),
    ja_signUpInput_email = document.getElementById("signUpInput_email"),
    ja_signUpInput_password = document.getElementById("signUpInput_password"),
    ja_signUpInput_confirmPassword = document.getElementById("signUpInput_confirmPassword"),
    ja_signUpSpan = document.getElementById("signUpSpan");

// 기타 변수 선언
let state;
// 사용자 정의 함수 선언

// 페이지 로드시 실행 코드

// 이벤트 등록
ja_signUpSecSpan.onclick = function () {
    ja_moveContainer.classList.toggle("signSec_pageTwo");
    this.innerHTML = (ja_moveContainer.classList.contains("signSec_pageTwo")) ? "◀ Sign In" : "Sign Up ▶";
};
ja_signInput_email.onkeyup = function (event) {
    ja_signModeSpan.innerHTML = checkSignInMode();
    let kCode = event.keyCode;
    let enterKey = kCode == 13;

    if (enterKey) {
        ja_signInSpan.click();
    }
};
ja_signInput_passwd.onkeyup = function (event) {
    ja_signModeSpan.innerHTML = checkSignInMode();
    if (state.verified) ja_signInSpan.innerHTML = "Sign In:";
    if (state.verified && this.value.length == 0) ja_signInSpan.innerHTML = "Sign Out:";
    let kCode = event.keyCode;
    let enterKey = kCode == 13;
    if (enterKey) {
        ja_signInSpan.click();
    }
};
ja_signInSpan.onclick = function () {
    if (state.verified) {
        if (state.status == "listening-mode" && ja_signInput_passwd.value.length != 0)
            requestPermission("edit-mode", { email: ja_signInput_email.value, pass: ja_signInput_passwd.value });
        else requestPermission("guest-mode");
    } else {
        // 이메일 주소인지 확인
        if (checkEmailAddress(ja_signInput_email.value)){
            if (ja_signInput_passwd.value.length == 0)
                requestPermission("listening-mode", { email: ja_signInput_email.value });
            else requestPermission("edit-mode", { email: ja_signInput_email.value, pass: ja_signInput_passwd.value });
        }
        else console.log("응 아니야");
    }
};

ja_signUpInput_email.onkeyup = function (event) {
    let kCode = event.keyCode;
    let enterKey = kCode == 13;
    if (checkEmailAddress(this.value)) {
        this.classList.remove("error");
        this.classList.add("checked");
        ja_signUpSpan.classList.add("emailChecked");
    }
    else {
        this.classList.remove("checked");
        ja_signUpSpan.classList.remove("emailChecked");
    }
    if (enterKey) ja_signUpSpan.click();
};
ja_signUpInput_password.onkeyup = function (event) {
    let kCode = event.keyCode;
    let enterKey = kCode == 13;
    if (this.value.length > 3) {
        this.classList.remove("error");
        this.classList.add("checked");
        ja_signUpSpan.classList.add("passwordChecked");
    }
    else {
        this.classList.remove("checked");
        ja_signUpSpan.classList.remove("passwordChecked");
    }
    if (enterKey) ja_signUpSpan.click();
};
ja_signUpInput_confirmPassword.onkeyup = function (event) {
    let kCode = event.keyCode;
    let enterKey = kCode == 13;
    if (ja_signUpInput_password.value == this.value) {
        this.classList.remove("error");
        this.classList.add("checked");
        ja_signUpSpan.classList.add("confirmPasswordChecked");
    }
    else {
        this.classList.remove("checked");
        ja_signUpSpan.classList.remove("confirmPasswordChecked");
    }
    if (enterKey) ja_signUpSpan.click();
};
ja_signUpSpan.onclick = function () {
    if (this.classList.contains("emailChecked") &&
        this.classList.contains("passwordChecked") &&
        this.classList.contains("confirmPasswordChecked")) {
        $.ajax({
            beforeSend: function(jqXHR){
                jqXHR.setRequestHeader("X-Requested-Permission", "sign-up");
            },
            method: "post",
            data: { email: ja_signUpInput_email.value, pass: ja_signUpInput_password.value },
            success: function (result) {
                state = result;
                console.log(state);
                if (state.msg == "계정 생성을 축하합니다.") ja_signUpSecSpan.click();
            },
            error: function (jqXHR, textStatus, errorThrown) { // 서버단 에러
                console.log(textStatus + " : " + errorThrown);
            }
        });
    } else {
        if (!this.classList.contains("emailChecked")) {
            ja_signUpInput_email.classList.add("error");
        }
        if (!this.classList.contains("passwordChecked")) {
            ja_signUpInput_password.classList.add("error");
        }
        if (!this.classList.contains("confirmPasswordChecked")) {
            ja_signUpInput_confirmPassword.classList.add("error");
        }
    }
};