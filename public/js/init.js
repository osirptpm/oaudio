/**
 * Created by Osirp on 2017-02-15.
 */

/*
 소켓 메세지
 data - text (message)
 - socket_id
 - type (json, string)
 Ajax 메세지
 email, msg, name, socketId, status, uid, verified
 detail
 - playedSong
 - randomArt
 */

// 이 페이지에서 사용될 함수들
/**
 * ajax 인증 요청
 * mode : listening-mode, edit-mode, guest-mode, current-state
 * @param {string} [mode = 'edit-mode']
 * @param {object} data
 */
function requestPermission(mode, data) {
    $.ajax({
        beforeSend: function(jqXHR){
            jqXHR.setRequestHeader("X-Requested-Permission", mode);
        },
        method: "post",
        data: data,
        success: function (result) {
            state = result;
            console.log(state);
            changeSignSec();
            changePlayingSec();
            /*
            var ja_signInput_email = document.getElementById("signInput_email");

            if (result.verified) { // 로그인 성공
                //console.log("성공");
                hashStateControl.add("playingSec");
                ja_signInput_email.readOnly = true;
                ja_signInput_email.type = "text";
                ja_signInput_email.value = "로그아웃";

                ja_oMusic.src = "users/" + state.name + "/WebPlayer/musics/" + state.playlist[0].filename;
                //ja_oMusic.play();

            } else { // 실패
                //console.log("실패");
                hashStateControl.remove("playingSec");
                ja_signInput_email.readOnly = false;
                ja_signInput_email.type = "email";
                ja_signInput_email.value = "";
            }*/
        },
        error: function (jqXHR, textStatus, errorThrown) { // 서버단 에러
            console.log(textStatus + " : " + errorThrown);
        }
    });
}

// 소켓 연결
function socket_connect() {
    var host = "ws://" + window.location.hostname + ":8100/";
    try {
        socket = new WebSocket(host);
        //console.log('WebSocket - status '+socket.readyState);
        socket.onopen = function(msg) {
            //console.log("Welcome - status "+this.readyState);
        };
        socket.onmessage = function(result) {
            var data = JSON.parse(result.data); // 받은 데이터를 json 으로
            /*
             data - text (message)
             - socket_id
             - type (json, string)
             */
            if (data.text == 'Success Handshake') { // 소켓 ID 등록
                // 소켓 ID 등록
                $.ajax({
                    beforeSend: function(jqXHR){
                        jqXHR.setRequestHeader("X-Send-SocketId");
                    },
                    method: "post",
                    data: { socketId: data.socketId },
                    success: function (result) {
                        console.log(result.msg);
                    },
                    error: function (jqXHR, textStatus, errorThrown) { // 서버단 에러
                        console.log(textStatus + " : " + errorThrown);
                    }
                });
            }
            else
            {
                console.log(JSON.parse(data.text));
            }
        };
        socket.onclose = function(msg) {
            //console.log("Disconnected - status "+this.readyState);
            console.log("푸시알림 연결이 해제되었습니다.");
        };
    }
    catch(ex) {
        console.log(ex);
    }
}

// 사용자 정의 함수 선언
var VCBHP = new ViewControlByHashParam(), // 주소값에 따른 DOM 컨트롤 함수
    VCBHPLoad = VCBHP.loadChainFunc(); // chainFunc 오브젝트 로드

// 페이지 로드시 실행 코드
socket_connect(); // 소켓 통신 요청
requestPermission("current-state"); // 현재 세션 상태 요청

// 이미지 블러처리
$(".blurEffect").each(function () {
    this.blurEffect();
});