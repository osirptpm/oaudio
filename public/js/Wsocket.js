/**
 * Created by Osirp on 2017-01-20.
 */
console.log("hi");
var socket;
var test_id;

function init(){
    var host = `ws://${location.host}:8100/`; // fixme websocket addr
    try{
        socket = new WebSocket(host);
        log('WebSocket - status '+socket.readyState);
        socket.onopen    = function(msg){ log("Welcome - status "+this.readyState); };
        socket.onmessage = function(msg){

            console.log(msg);
            console.log(JSON.parse(msg.data));

            var data = JSON.parse(msg.data);
            test_id = data.socket_id;
            log("Received: "+data.text);
        };
        socket.onclose   = function(msg){ log("Disconnected - status "+this.readyState); };
    }
    catch(ex){ log(ex); }
    document.getElementById("msg").focus();
}

function send(){
    var txt,msg;
    txt = document.getElementById("msg");
    msg = txt.value;
    if(!msg){ alert("Message can not be empty"); return; }
    txt.value="";
    txt.focus();
    try{ socket.send(msg); log('Sent: '+msg); } catch(ex){ log(ex); }
}
function quit(){
    log("Goodbye!");
    socket.close();
    socket=null;
}

// Utilities
//function $(id){ return document.getElementById(id); }
function log(msg){ document.getElementById("log").innerHTML+="<br>"+msg; }
function onkey(event){ if(event.keyCode==13){ send(); } }

function test() {
    $.ajax({
        beforeSend: function(jqXHR){
            jqXHR.setRequestHeader("X-Requested-Permissions");
        },
        method: "post",
        data: {socketId: test_id},
        success: function (result) {
            console.log(result);
        },
        error: function (jqXHR, textStatus, errorThrown) { // 서버단 에러
            console.log(textStatus + " : " + errorThrown);
        }
    });
}