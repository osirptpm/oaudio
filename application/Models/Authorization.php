<?php
/**
 * Created by IntelliJ IDEA.
 * User: Osirp
 * Date: 2017-02-04
 * Time: 오전 10:57
 */

namespace oMusic\application\Models;



use oMusic\application\Controllers\Controller;

class Authorization
{
    private $connection;

    function __construct()
    {
        require_once('Etc/CV.php');
        $this->connection = new \PDO('mysql:host=' . HOST . ';dbname=' . DB_OMUSIC . ';charset=utf8', NAME, PASS);
    }

    function run()
    {
        if ($_SERVER['HTTP_X_REQUESTED_PERMISSION'] == 'current-state')
        {
            Controller::$state->msg('현재 상태입니다.');
            return true;
        }
        else if ($_SERVER['HTTP_X_REQUESTED_PERMISSION'] == 'listening-mode' || $_SERVER['HTTP_X_REQUESTED_PERMISSION'] == 'edit-mode') // 듣기, 편집 모드 로그인
        {
            return $this->listeningMode();
        }
        else if ($_SERVER['HTTP_X_REQUESTED_PERMISSION'] == 'guest-mode') // 게스트 모드
        {
            return $this->guestMode();
        }
        else if ($_SERVER['HTTP_X_REQUESTED_PERMISSION'] == 'sign-up') // 게스트 모드
        {
            return $this->signUp();
        }
        return false;
    }

    /**
     * @param {string} $email
     * @return bool
     */
    private function listeningMode()
    {
        try {
            if ($_SERVER['HTTP_X_REQUESTED_PERMISSION'] == 'listening-mode')
            {
                $statement = $this->connection->prepare('SELECT uid, email FROM ' . T_MEMVERS . ' WHERE email = :email');
                $statement->execute([
                    ':email' => $_POST['email']
                ]);
            } else if ($_SERVER['HTTP_X_REQUESTED_PERMISSION'] == 'edit-mode')
            {
                $statement = $this->connection->prepare('SELECT uid, email FROM ' . T_MEMVERS . ' WHERE email = :email AND pass = SHA2(:pass, 224)');
                $statement->execute([
                    ':email' => $_POST['email'],
                    ':pass' => $_POST['pass']
                ]);
            }

            $row = $statement->fetchALL(\PDO::FETCH_OBJ);
            if (count($row) == 1) {
                $_SESSION['uid'] = $row[0]->uid;
                $_SESSION['email'] = $row[0]->email;
                $_SESSION['status'] = $_SERVER['HTTP_X_REQUESTED_PERMISSION'];

                Controller::$state->signIn();
            }
            else
            {
                Controller::$state->msg('이메일 주소나 비밀번호가 틀렸습니다.');
            }
            return true;
        }
        catch (\PDOException $e)
        {
            return false;
        }
    }

    private function guestMode()
    {
        session_unset();
        if(!session_destroy()) return false;
        Controller::$state->signOut();
        //echo Controller::$state;
        return true;
    }

    private function signUp()
    {
        try {
            $statement = $this->connection->prepare('SELECT COUNT(email) FROM ' . T_MEMVERS . ' WHERE email = :email');
            $statement->execute([
                ':email' => $_POST['email']
            ]);
            if ($statement->fetchColumn() > 0) {
                Controller::$state->msg('사용 중인 이메일 주소입니다.');
                return true;
            }
            $statement = $this->connection->prepare('INSERT INTO ' . T_MEMVERS . ' (email, pass) VALUES (:email, SHA2(:pass, 224))');

            $statement->bindParam(':email', $_POST['email']);
            $statement->bindParam(':pass', $_POST['pass']);
            $statement->execute();
            // print_r($statement->errorInfo());
            // echo ('INSERT INTO ' . T_MEMVERS . ' (email, pass) VALUES (:email, PASSWORD(:pass))');
            if ($statement->rowCount() == 1) Controller::$state->msg('계정 생성을 축하합니다.');
            else Controller::$state->msg('가입 에러. 관리자에게 문의해 주시기 바랍니다.');
            return true;
        }
        catch (\PDOException $e)
        {
            return false;
        }
    }
}



//$test = new Authorization();
//$test->signin('osi@gmail.com');