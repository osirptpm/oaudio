<?php
/**
 * Created by IntelliJ IDEA.
 * User: Osirp
 * Date: 2017-02-08
 * Time: 오전 11:31
 */

namespace oMusic\application\Models;


use oMusic\application\Controllers\Controller;

class Socket
{
    private $address, $port, $socket, $socketId;
    function __construct()
    {
        error_reporting(E_ALL);
        $this->address = getHostByName(getHostName());
        $this->port = 8100; //fixme 웹소켓 접속할 PORT
    }

    function run()
    {
        $this->socketId = Controller::$state->getSocketId();

        $this->socket = @socket_create(AF_INET, SOCK_STREAM, SOL_TCP);// or die('socket_create() failed');
        $result = @socket_connect($this->socket, $this->address, $this->port);// or die('socket_connect() failed'); // 소켓 연결 및 $result에 접속값 지정
    }
    function runForScript($socketId)
    {
        $this->socketId = $socketId;

        $this->socket = @socket_create(AF_INET, SOCK_STREAM, SOL_TCP);// or die('socket_create() failed');
        $result = @socket_connect($this->socket, $this->address, $this->port);// or die('socket_connect() failed'); // 소켓 연결 및 $result에 접속값 지정
    }

    function sendMsg($text = null)
    {
        $data = new \stdClass();
        $data->request_from = 'php';
        $data->socketId = $this->socketId;
        $data->msg = $text;

        @socket_write($this->socket, json_encode($data) . '&&&&');
    }
    function disconnect()
    {
        @socket_close($this->socket);
    }
}