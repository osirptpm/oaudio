<?php
/**
 * Created by IntelliJ IDEA.
 * User: Osirp
 * Date: 2017-02-09
 * Time: 오전 11:11
 */

namespace oMusic\application\Models;


class State implements \JsonSerializable
{
    private $socketId, $verified, $status, $msg, $email, $uid, $flag, $detail; // json 리턴
    private $playlist; //
    function __construct()
    {
        $this->detail = new \stdClass();
        $this->detail->randomArt = null;
        $this->detail->pendingFileId = null;
        $this->detail->pendingFileDuration = null;
        $this->detail->randomArt = null;
        $this->detail->path = '/users/NEWUSER/WebPlayer/';
        $this->verified = false;
        $this->status = 'guest-mode';
        $this->flag = 'server';
    }

    function run()
    {
        if (isset($_SESSION['socketId'])) $this->socketId = $_SESSION['socketId'];
        $this->signIn();
    }
    function runForScript($json) // json
    {
        $state = json_decode($json);

        $this->socketId = $state->socketId;
        $this->status = $state->status;
        $this->uid = $state->uid;
        $this->email = $state->email;
        $this->verified = $state->verified;
        $this->msg = null;
    }

    function setSocketId()
    {
        $this->msg = '푸시알림 ID가 등록되었습니다.';
        $_SESSION['socketId'] = (empty($_POST['socketId'])) ? null : $_POST['socketId'];
        $this->socketId = (empty($_POST['socketId'])) ? null : $_POST['socketId'];
    }
    function setFlag($value)
    {
        $this->flag = $value;
    }
    function setPlaylist($playlist)
    {
        $this->playlist = $playlist;
    }
    function setDetail($key, $value)
    {
        $this->detail->$key = $value;
    }
    function getStatus()
    {
        return $this->status;
    }
    function getVerified()
    {
        return $this->verified;
    }
    function getUid()
    {
        return $this->uid;
    }
    function getEmail()
    {
        return $this->email;
    }
    function getSocketId()
    {
        return $this->socketId;
    }
    function getPlaylist()
    {
        return $this->playlist;
    }
    function getDetail($key)
    {
        return $this->detail->$key;
    }

    function msg($text)
    {
        $this->msg = $text;
    }
    function signIn()
    {
        if (isset($_SESSION['uid']))
        {
            $this->status = $_SESSION['status'];
            $this->uid = $_SESSION['uid'];
            $this->email = $_SESSION['email'];
            $this->verified = ($this->status == 'guest-mode' || $this->status == null) ? false : true;
            $this->detail->path = '/users/' . $_SESSION['email'] . '/WebPlayer/';
            $this->msg = '환영합니다.';
        }
    }
    function signOut()
    {
        $this->status = 'guest-mode';
        $this->uid = null;
        $this->email = null;
        $this->playlist = null;
        $this->verified = false;
        $this->msg = '로그아웃 하셨습니다.';
        $this->detail->playedSong = null;
        $this->detail->randomArt = null;
    }

    function jsonSerialize()
    {
        return [
            'verified' => $this->verified,
            'status' => $this->status,
            'msg' => $this->msg,
            'uid' => $this->uid,
            'email' => $this->email,
            'socketId' => $this->socketId,
            'detail' => $this->detail,
            'flag' => $this->flag,
            'playlist' => $this->playlist

        ];
    }
}