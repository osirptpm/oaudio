<?php
/**
 * Created by IntelliJ IDEA.
 * User: Osirp
 * Date: 2017-02-11
 * Time: 오전 10:31
 */

if (count($argv) < 2) return false;
$argv[1] = str_replace('\\\'', '\'', $argv[1]);
$state = json_decode($argv[1]);
//var_dump($state);
$usersDir = $state->detail->usersDir; // "E:\Users/"
$uid = $state->uid; // uid
$email = $state->email; // 유저 ID
$socketId = $state->socketId; // 소켓 ID

$countdown = 5; // 5초동안 인코딩 대기

$curPath = realpath(dirname(__FILE__)); // 현재 파일 경로
$modelPath = strstr($curPath, 'Script', true); // Models 폴더 경로
// 유저 상태 정의
require_once($modelPath . 'State.php');
$state = new \oMusic\application\Models\State();
$state->runForScript($argv[1]);
$state->setFlag('fileEncoding');

// 웹 소켓 접속
require_once($modelPath . 'Socket.php');
$socket = new \oMusic\application\Models\Socket();
$socket->runForScript($socketId);

for ($i = $countdown; $i >  0; $i--)
{
    $state->msg($i . '초 후에 변환 시작');
    $socket->sendMsg(json_encode($state));
    sleep(1);
}
try {
    require_once($modelPath . 'Etc/CV.php');
    $connection = new PDO('mysql:host=' . HOST . ';dbname=' . DB_OMUSIC . ';charset=utf8', NAME, PASS);
    // 인코딩 중인지
    $statement = $connection->prepare("SELECT encoding FROM " . T_MEMVERS ." WHERE uid = :uid");
    $statement->execute([':uid' => $uid]);
    $isEncoding = $statement->fetchColumn();

    // 인코딩 중이라면 그냥 종료
    if ($isEncoding) {
        $state->msg('이미 인코딩 중');
        $socket->sendMsg(json_encode($state));
        return false;
    }
    //$socket->sendMsg('인코딩 시작');

    // 인코딩 중으로 상태 변경
    $statement = $connection->prepare("UPDATE " . T_MEMVERS ." SET encoding = 1 WHERE uid = :uid");
    $statement->bindParam(':uid', $uid);
    $statement->execute();
    $count = $statement->rowCount(); // 변경된 행 수
    if ($count != 1) return false; // 인코딩 중으로 설정 실패 -> 중대 오류
    
    while (true)
    {
        // 인코딩 대기 중인 목록 출력
        $statement = $connection->prepare('SELECT pid, filename FROM ' . T_PENDINGFILES . ' WHERE uid = :uid LIMIT 1');
        $statement->execute([
            ':uid' => $uid
        ]);
        $pending_file = $statement->fetchObject();

        // 대기 목록이 비었을 때
        if (empty($pending_file)) break;

        $etcPath = $modelPath . 'Etc\\';
        $ffmpegPath = $etcPath . 'ffmpeg.exe';
        $ffprobePath = $etcPath . 'ffprobe.exe';

        // 인코딩 시작전 파일 정보 요청 (앨범 아트, 태그 정보)
        $filenameWithoutExt = substr($pending_file->filename, 0, strrpos($pending_file->filename, '.')); // 확장자 제거

        $uploadPath = $usersDir . $email . '/WebPlayer/upload/';
        $coverPath = $usersDir . $email . '/WebPlayer/covers/';
        $logsPath = $usersDir . $email . '/WebPlayer/logs/';
        $musicsPath = $usersDir . $email . '/WebPlayer/musics/';

        // 원본 파일
        $inputPath = $uploadPath . $pending_file->filename;

        // 앨범아트 출력 (오디오 x, 비디오 복사, 덮어쓰기 옵션, 에러 출력 x)
        $jpgPreset = ' -an -c:v copy ';
        $jpgOutput = $coverPath . $filenameWithoutExt . '.jpg';
        exec($ffmpegPath . ' -i ' . escapeshellarg($inputPath) . $jpgPreset . escapeshellarg($jpgOutput) . ' -y 2>nul');
        //$socket->sendMsg('앨범아트 추출완료');

        // 태그 정보 json로 출력 (format 안에 tags가 있음)
        $tagsPreset = ' -print_format json -show_entries format ';
        $infoOutput = $logsPath . $filenameWithoutExt . '.info';
        exec($ffprobePath . $tagsPreset . escapeshellarg($inputPath) . ' > ' . escapeshellarg($infoOutput) . ' 2>nul');
        //$socket->sendMsg('태그정보파일 추출완료');

        $msg = new stdClass();
        $msg->flag = 'fileInfo';

        $infoFile = @file_get_contents($infoOutput);
        if ($infoFile !== false) {
            $msg->file_info = json_decode($infoFile);
            $msg->file_info->format->filename = $pending_file->filename;
            $msg->file_info->format->cover = (file_exists($jpgOutput)) ? true : false;
            //$state->msg('태그정보 추출완료');
            //$socket->sendMsg(json_encode($state));
        }
        else
        {
            $state->msg('태그정보 추출실패');
            $socket->sendMsg(json_encode($state));
        }

        $msg->file_info->format->tags = (object) array_change_key_case((array) $msg->file_info->format->tags, CASE_LOWER); // 키값 소문자로
        $format = $msg->file_info->format;
        $filenameMp3 = $filenameWithoutExt . '.mp3';
        $duration = floor($format->duration);

        // 깨진 글자 복구
        $title = characterRecovery($format->tags->title);
        $artist = characterRecovery($format->tags->artist);
        $album = characterRecovery($format->tags->album);
        $genre = characterRecovery($format->tags->genre);
        $lyrics = characterRecovery($format->tags->lyrics);

        // 곡 정보 DB에 등록
        $statement = $connection->prepare("INSERT INTO " . T_SONGINFO . " (uid, filename, title, artist, album, duration, genre, lyrics, albumart) VALUES (:uid, :filename, :title, :artist, :album, :duration, :genre, :lyrics, :albumart) ON DUPLICATE KEY UPDATE title=:title, artist=:artist, album=:album, duration=:duration, genre=:genre, lyrics=:lyrics, albumart=:albumart");
        $statement->bindParam(':uid', $uid);
        $statement->bindParam(':filename', $filenameMp3);
        $statement->bindParam(':title', $title);
        $statement->bindParam(':artist', $artist);
        $statement->bindParam(':album', $album);
        $statement->bindParam(':duration', $duration);
        $statement->bindParam(':genre', $genre);
        $statement->bindParam(':lyrics', $lyrics);
        $statement->bindParam(':albumart', $format->cover, PDO::PARAM_BOOL);
        $statement->execute();

        if (strrchr($pending_file->filename, '.') == '.mp3') // mp3 파일이면
        {
            @unlink($infoOutput); // 로그 파일 삭제
            if (!copy($inputPath, $musicsPath . $pending_file->filename)) {
                $state->msg('실제 파일 전송 실패');
                $socket->sendMsg(json_encode($state));
            }
            //@unlink($progressOutput); // 시작도 안함
        }
        else
        {
            // 인코딩 진행 상황 가져오기
            // 백그라운드 프로세스로 요청
            $fileInfoDuringEncoding = escapeshellarg($curPath . '\\fileInfoDuringEncoding.php');
            // json 으로 인자 생성
            $state->setDetail('pendingFile', $pending_file->pid); // 파일 ID
            $state->setDetail('pendingFileDuration', floor($format->duration * 10)); // 재생 시간
            $state->setDetail("usersDir", $usersDir);

            $args = ' "' . addslashes(json_encode($state)) . '"';

            $WshShell = new COM("WScript.Shell");
            $WshShell->Run('php ' . $fileInfoDuringEncoding . $args, 0, false);

            // 파일 인코딩 요청s
            $presets = ' -f mp3 -id3v2_version 3 -write_id3v1 1 -b:a 192k -c:v copy '; // 태그 정보 가지고 변환
            $outputPath = $musicsPath . $filenameWithoutExt . '.mp3';
            $progressPath = $logsPath . $filenameWithoutExt . '.progress';
            $cmd = $modelPath . 'Etc/ffmpeg.exe -i ' . escapeshellarg($inputPath) . $presets . escapeshellarg($outputPath) . ' -progress ' . escapeshellarg($progressPath) . ' -y'; // 덮어쓰기
            exec($cmd, $output); // 인코딩
        }

        // 인코딩 끝난 파일 목록에서 제거
        $statement = $connection->prepare('DELETE FROM ' . T_PENDINGFILES . ' WHERE pid = :pid');
        $statement->execute([
            ':pid' => $pending_file->pid
        ]);
        //sleep(5);

        $movePath = $usersDir . $email . '/WebPlayer/original_file/' . $pending_file->filename;
        rename($inputPath, $movePath); // 원본파일 백업

    }

    // 인코딩 마침으로 상태 변경
    $statement = $connection->prepare("UPDATE " . T_MEMVERS ." SET encoding = 0 WHERE uid = :uid");
    $statement->bindParam(':uid', $uid);
    $statement->execute();
}
catch(Exception $e)
{
    $result['sql_error'] = 'DB 에러';//$e;
}
//$state->msg('인코딩 끝');
//$socket->sendMsg(json_encode($state));


// 깨진 글자 복구
function characterRecovery($str) {
    if (empty($str)) return $str;
    $bin = str2bin(iconv('utf-8', 'utf-16le', $str));
    $bin_len = count($bin);
    $bin2 = [];
    for ($i = 0; $i < $bin_len; $i+=2)
    {
        $bin2[] = $bin[$i];
    }
    $fixed = @iconv('euc-kr', 'utf-8', bin2str($bin2));
    if ($fixed === false)
    {
        return $str;
    }
    else
    {
        return $fixed;
    }
}
function str2bin($input) {
    $result = [];
    $arr = str_split($input);
    foreach ($arr as $i)
    {
        $result[] = str_pad(decbin(ord($i)), 8, '0', STR_PAD_LEFT);
    }
    return $result;
}
function bin2str($input) {
    if (gettype($input) == 'array') $input = implode($input);
    $result = '';
    $arr = str_split($input, 8);
    foreach ($arr as $i)
    {
        $result .= chr(bindec($i));
    }
    return $result;
}