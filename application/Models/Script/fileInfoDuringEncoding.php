<?php
/**
 * Created by IntelliJ IDEA.
 * User: Osirp
 * Date: 2017-02-11
 * Time: 오후 12:05
 */
if (count($argv) < 2) return false;
$argv[1] = str_replace('\\\'', '\'', $argv[1]);
$state = json_decode($argv[1]);
$usersDir = $state->detail->usersDir; // "E:\Users/"
$uid = $state->uid; // uid
$email = $state->email; // 유저 ID
$socketId = $state->socketId; // 소켓 ID
$pid = $state->detail->pendingFile; // 파일 이름
$duration = $state->detail->pendingFileDuration; // 재생 시간

$curPath = realpath(dirname(__FILE__)); // 현재 파일 경로
$modelPath = strstr($curPath, 'Script', true); // Models 폴더 경로 \ 포함

// 유저 상태 정의
require_once($modelPath . 'State.php');
$state = new \oMusic\application\Models\State();
$state->runForScript($argv[1]);
$state->setFlag('fileEncoding');

// 웹 소켓 접속
require_once($modelPath . 'Socket.php');
$socket = new \oMusic\application\Models\Socket();
$socket->runForScript($socketId);

require_once($modelPath . 'Etc/CV.php');
$connection = new PDO('mysql:host=' . HOST . ';dbname=' . DB_OMUSIC . ';charset=utf8', NAME, PASS);
// 인코딩 대기 중인 파일 가져오기
$statement = $connection->prepare('SELECT filename FROM ' . T_PENDINGFILES . ' WHERE uid = :uid AND pid=:pid LIMIT 1');
$statement->execute([
    ':uid' => $uid,
    ':pid' => $pid
]);
$pending_file = $statement->fetchColumn();

$uploadPath = $usersDir . $email . '/WebPlayer/upload/';
$logsPath = $usersDir . $email . '/WebPlayer/logs/';

if (empty($pending_file)) // 대기 중인 파일이 없다고 나올경우
{
    sleep(1);
    $infoOutput = $logsPath . $filenameWithoutExt . '.info';
    @unlink($infoOutput);
    //@unlink($progressOutput);
    return;
}
$filename = $pending_file;
$filenameWithoutExt = substr($filename, 0, strrpos($filename, '.')); // 확장자 제거

// 원본 파일
$input = $uploadPath . $filename;

// 인코딩 진행정보 가져오기
$encodingProgress = new stdClass();
$encodingProgress->flag = 'fileEncoding';
$encodingProgress->file_name = $filename;
$encodingProgress->duration = $duration;
$encodingProgress->progress = 0;
$encodingProgress->detail = null;
$encodingProgress->error = (empty($encodingProgress->duration)) ? true : false;
//$state->msg('인코딩 진행정보 가져오기');
//$socket->sendMsg(json_encode($state));
$progressOutput = $logsPath . $filenameWithoutExt . '.progress';
while (true)
{
    if (file_exists($input)) // 원본 파일이 남아 있다면 -> 변환 중
    {
        $progress = @file_get_contents($progressOutput);
        if ($progress !== false)
        {
            $state->msg('인코딩 중');
            if (empty($progress))
            {
                //$socket->sendMsg('인코딩 진행정보 없음');
                $state->setDetail('encodingProgress', $encodingProgress);
            }
            else
            {
                $encodingProgress->detail = editText($progress);
                if (!$encodingProgress->error) $encodingProgress->progress = 100 * substr($encodingProgress->detail->out_time_ms, 0, -5) / $encodingProgress->duration;
                $state->setDetail('encodingProgress', $encodingProgress);
            }
        }
        else
        {
            $state->msg('인코딩 진행파일 없음');
        }
        $socket->sendMsg(json_encode($state));
        usleep(500000);
    } else break;
}
//$socket->sendMsg('끝');
$encodingProgress->progress = 100;
$state->setDetail('encodingProgress', $encodingProgress);
$socket->sendMsg(json_encode($state));

sleep(1);
$infoOutput = $logsPath . $filenameWithoutExt . '.info';
@unlink($infoOutput);
@unlink($progressOutput);


function editText($text)
{
    $index = strrpos($text, 'frame=1');
    $result = trim(substr($text, $index));
    $result = str_replace("=", "\":\"", $result);
    $result = str_replace("\n", "\",\"", $result);
    return json_decode('{"'. $result . '"}');
}