<?php
/**
 * Created by IntelliJ IDEA.
 * User: Osirp
 * Date: 2017-02-13
 * Time: 오전 7:43
 */

namespace oMusic\application\Models;


use oMusic\application\Controllers\Controller;

class Playlist
{
    private $totalPlaylist;
    private $customPlaylist;

    function run()
    {
        // 로그인 하지않은 상태라면 종료
        if (!Controller::$state->getVerified())
        {
            $this->totalPlaylist = null;
            return true;
        }

        require_once('Etc/CV.php');
        $connection = new \PDO('mysql:host=' . HOST . ';dbname=' . DB_OMUSIC . ';charset=utf8', NAME, PASS);
        $uid =  Controller::$state->getUid();
        try {
            $statement = $connection->prepare('SELECT * FROM ' . T_SONGINFO . ' WHERE uid = :uid');
            $statement->execute([
                ':uid' => $uid
            ]);

            $rows = $statement->fetchALL(\PDO::FETCH_OBJ);
            foreach ($rows as $row) // 파일경로 입력
            {
                $row->src = Controller::$state->getDetail('path') . 'musics/' . $row->filename;
                $albumArtSrc = Controller::$state->getDetail('path') . 'covers/';
                if ($row->albumArt == 1)
                    $albumArtSrc .= substr($row->filename, 0, strrpos($row->filename, '.')) . '.jpg';
                else $albumArtSrc .= 'sample/sample.jpg';
                $row->albumArtSrc = $albumArtSrc;
            }
            $this->totalPlaylist = $rows;
            Controller::$state->setDetail('randomArt', $this->_picRandomAlbumArt());
            return true;
        }
        catch (\PDOException $e)
        {
            return false;
        }
    }

    function getTotalPlaylist()
    {
        return $this->totalPlaylist;
    }
    function getPlaylist()
    {
        $playlist = new \stdClass();
        $playlist->totalPlaylist = $this->totalPlaylist;
        $playlist->customPlaylist = $this->customPlaylist;
        $playlist->currentPlaylist = null;
        return $playlist;
    }

    function _picRandomAlbumArt()
    {
        $albumArtSrc = Controller::$state->getDetail('path') . 'covers/';
        $Index = [];
        $total_len = count($this->totalPlaylist);
        for ($i = 0; $i < $total_len; $i++)
        {
            if ($this->totalPlaylist[$i]->albumArt) $Index[] = $i;
        }
        $Count = count($Index);
        if ($Count == 0)
        {
            $albumArtSrc .= 'sample/sample.jpg';
        }
        else
        {
            $song = $this->totalPlaylist[$Index[rand(0, $Count - 1)]];
            $albumArtSrc .= substr($song->filename, 0, strrpos($song->filename, '.')) . '.jpg';
        }
        return $albumArtSrc;
    }
}