<?php
/**
 * Created by IntelliJ IDEA.
 * User: Osirp
 * Date: 2017-02-06
 * Time: 오후 5:28
 */

namespace oMusic\application\Models;



use oMusic\application\Controllers\Controller;

class Upload
{
    private $usersDir, $userDir, $webPlayerDir, $uploadDir, $logsDir;
    function __construct()
    {
        $this->usersDir = 'D:/Users/'; //fixme 서버 이전시 경로 체크
        $this->userDir = $this->usersDir . Controller::$state->getEmail() . '/';
        $this->webPlayerDir = $this->userDir . 'WebPlayer/';
        $this->uploadDir = $this->userDir . 'WebPlayer/upload/';
        $this->logsDir = $this->userDir . 'WebPlayer/logs/';
        $this->musics = $this->userDir . 'WebPlayer/musics/';
        $this->original_file = $this->userDir . 'WebPlayer/original_file/';
        $this->covers = $this->userDir . 'WebPlayer/covers/';
    }

    function run()
    {
        if(Controller::$state->getStatus() == 'edit-mode')
        {
            return $this->audioFileUpload();
        }
        else
        {
            Controller::$state->msg('편집 모드가 아닙니다.');
            return true;
        }
    }

    function audioFileUpload()
    {
        if (!empty($_FILES['audioFile']) && !empty($_FILES['audioFile']['name'][0])) // 파일이 존재하면
        {
            // 폴더가 없을때 만들어주는 코드
            if (!file_exists($this->covers) || !file_exists($this->uploadDir) || !file_exists($this->logsDir) || !file_exists($this->musics) || !file_exists($this->original_file))  $this->recurse_copy($_SERVER['DOCUMENT_ROOT'] . '/application/Models/Etc/NEWUSER/', $this->userDir);

            $result = [];
            $result['state'] = true;
            $files = $_FILES['audioFile'];

            $files_len = count($files['name']);
            for ($i = 0; $i < $files_len; $i++)
            {
                $fileExt = strrchr($files['name'][$i], '.');
                if (explode('/', $files['type'][$i])[0] == 'audio' && $this->isAudioFile($fileExt) !== false)
                {
                    // 업로드 폴더로 이동
                    move_uploaded_file($files['tmp_name'][$i], $this->uploadDir.$files['name'][$i]);
                    $pendingFiles[] = $files['name'][$i]; // 인코딩할 파일
                }
                else
                {
                    $files['error'][$i] = '오디오 파일이 아닙니다.';
                }
            }
            unset($files['tmp_name']);
            $result = array_merge($result, $files);
            
            $pendingFiles_len = count($pendingFiles); // 등록할 파일이 없으면 종료
            if ($pendingFiles_len <= 0) {
                Controller::$state->msg('등록 가능한 파일이 없습니다.');
                return true;
            }

            try {
                require_once('Etc/CV.php');
                $connection = new \PDO('mysql:host=' . HOST . ';dbname=' . DB_OMUSIC . ';charset=utf8', NAME, PASS);

                // 업로드 된 파일을 인코딩 대기 DB에 저장
                $placeholder = array_fill(0, $pendingFiles_len, '(?,?)'); // '(?,?),(?,?),(?,?)'
                $placeholder = implode(',', $placeholder);
                $uid= Controller::$state->getUid();
                $statement = $connection->prepare("INSERT IGNORE INTO " . T_PENDINGFILES . " (uid, filename) VALUES $placeholder");

                for ($i = 1, $j = 0; $j < $pendingFiles_len; $i += 2, $j++) {
                    $statement->bindParam($i,  $uid);
                    $statement->bindParam($i + 1, $pendingFiles[$j]);
                }

                $statement->execute();

                // 인코딩 대기 중인 목록 출력
                $statement = $connection->prepare('SELECT filename FROM ' . T_PENDINGFILES . ' WHERE uid = :uid');

                $statement->execute([
                    ':uid' =>  $uid
                ]);

                $rows = $statement->fetchALL(\PDO::FETCH_ASSOC);

                foreach ($rows as $row)
                {
                    $result['pending_files'][] = $row['filename'];
                }
            }
            catch(\PDOException $e)
            {
                return false;
            }

            // 인코딩 프로세스 호출
            // 백그라운드 프로세스로 요청
            $EncodePendingFiles = escapeshellarg($_SERVER['DOCUMENT_ROOT'] . '\\application\\Models\\Script\\EncodePendingFiles.php');

            // json 으로 인자 생성
            Controller::$state->msg(null); // msg 비우기
            Controller::$state->setDetail("usersDir", $this->usersDir);
            $args = ' "' . addslashes(json_encode(Controller::$state)) . '"';

            //$WshShell = new \COM("WScript.Shell");
            $WshShell = null;
            if(!($WshShell = new \COM("WScript.Shell"))) {
                $result['errorMsg'] = $WshShell;
            } else {
                $result['msg'] = 'php ' . $EncodePendingFiles . $args;
            }
            $WshShell->Run('php ' . $EncodePendingFiles . $args, 0, false);

            Controller::$state->msg($result);
            return true;
        }
        else // 업로드 파일 존재하지 않음?
        {
            Controller::$state->msg('업로드된 파일이 없습니다.');
            return true;
        }
    }

    private function recurse_copy($src,$dst)
    {
        $dir = opendir($src);
        @mkdir($dst);
        while(false !== ($file = readdir($dir)))
        {
            if (($file != '.') && ($file != '..'))
            {
                if (is_dir($src . '/' . $file))
                {
                    $this->recurse_copy($src . '/' . $file,$dst . '/' . $file);
                }
                else
                {
                    copy($src . '/' . $file,$dst . '/' . $file);
                }
            }
        }
        closedir($dir);
    }
    private function isAudioFile($fileExt)
    {
        $exts = array( '.mp3', '.flac', '.wav', '.wma', '.m4a');
        return array_search($fileExt, $exts);
    }
}
//new Upload();