<?php
/**
 * Created by IntelliJ IDEA.
 * User: Osirp
 * Date: 2017-01-20
 * Time: 오후 7:03
 */

namespace oMusic\application\Views;


class Body
{
    function __construct()
    {
        //$this->webSocket();
        $this->oMusic();
    }

    private function oMusic()
    {
?>
    <body>
        <article id="oMusicArt" class=""> <!-- playingSec -->
<?php
            new SignSec();
            //new Audio();
            new PlayingSec();
?>
        </article>
        <script src="/public/js/oFunctions-0.2.js"></script>
        <script src="/public/js/init.js"></script>
        <script src="/public/js/SignSec.js"></script>
        <script src="/public/js/PlayingSec.js"></script>
        <script src="/public/js/UploadSec.js"></script>
    </body>
<?php
    }
    private function webSocket()
    {
        ?>
        <body onload="init()">
        <h3>WebSocket</h3>
        <div id="log"></div>
        <input id="msg" type="textbox" onkeypress="onkey(event)"/>
        <button onclick="send()">Send</button>
        <button onclick="quit()">Quit</button>
        <button onclick="test()">test</button>
        <div>Commands: hello, hi, name, age, date, time, thanks, bye</div>
        </body>
        <?php
    }
}