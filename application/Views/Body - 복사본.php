<?php
/**
 * Created by IntelliJ IDEA.
 * User: Osirp
 * Date: 2017-01-20
 * Time: 오후 7:03
 */

namespace oMusic\application\Views;


class Bodyback
{
    function __construct()
    {
        ?>
        <body onload="init()">
        <h3>WebSocket</h3>
        <div id="log"></div>
        <input id="msg" type="textbox" onkeypress="onkey(event)"/>
        <button onclick="send()">Send</button>
        <button onclick="quit()">Quit</button>
        <div>Commands: hello, hi, name, age, date, time, thanks, bye</div>
        </body>
        </html>
        <?php
    }
}