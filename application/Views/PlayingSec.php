<?php
/**
 * Created by IntelliJ IDEA.
 * User: Osirp
 * Date: 2017-02-01
 * Time: 오전 8:38
 */

namespace oMusic\application\Views;




class PlayingSec
{
    function __construct()
    {
?>
            <section id="playingSec" class="boardSec">
                <div id="playingDiv" class="flexCenterWrap" data-src="">
                    <canvas id="previousCanvas_btn" class="control_btn" width="75" height="150"></canvas>
                    <div id="infoDiv">
                        <div style="height: 50px;"></div>
                        <div id="albumArt">
                            <section id="playlistSec">
                            </section>
                        </div>
                        <div id="currentSongInfoDiv">
                            <span id="currentSongInfo_name"></span>
                            <span id="currentSongInfo_artist"></span>
                            <canvas id="progressCanvas" width="420" height="20"></canvas>
                        </div>
                    </div>
                    <canvas id="nextCanvas_btn" class="control_btn" width="75" height="150"></canvas>
                </div>
                <?php new UploadSec(); ?>
            </section>
<?php
    }
}