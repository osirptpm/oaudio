<?php
/**
 * Created by IntelliJ IDEA.
 * User: Osirp
 * Date: 2017-01-20
 * Time: 오후 7:12
 */

namespace oMusic\application\Views;


class Headback
{
    function __construct()
    {
        ?>
        <!DOCTYPE html>
        <html>
        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, height=device-height, user-scalable=no">
            <meta name="mobile-web-app-capable" content="yes">
            <meta name="theme-color" content="rgba(52, 152, 219, 1)">
            <meta name="application-name" content="oMusic">
            <title>oMusic</title>
            <link href="/public/css/main.css" rel="stylesheet">
            <!--<script src="/public/js/jquery-3.1.1.min.js"></script>-->
            <script src="/public/js/main.js"></script>
        </head>
        <?php
    }
}