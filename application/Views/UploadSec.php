<?php
/**
 * Created by IntelliJ IDEA.
 * User: Osirp
 * Date: 2017-02-06
 * Time: 오후 3:01
 */

namespace oMusic\application\Views;


class UploadSec
{
    function __construct()
    {
?>
        <section id="uploadSec">
            <div id="upload_btn">
                <canvas id="uploadCanvas" width="70" height="70"></canvas>
            </div>
            <form id="uploadForm">
                <input id="fileInput" type="file" name="audioFile[]" accept="audio/*" multiple>
            </form>
        </section>
<?php
    }
}

