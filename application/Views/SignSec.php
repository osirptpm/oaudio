<?php
/**
 * Created by IntelliJ IDEA.
 * User: Osirp
 * Date: 2017-02-06
 * Time: 오전 6:16
 */

namespace oMusic\application\Views;


use oMusic\application\Controllers\Controller;

class SignSec
{
    function __construct()
    {
?>
            <section id="signSec" class="boardSec">
                <div id="signDiv" class="flexCenterWrap blurEffect" data-src="/public/images/S/technology-music-sound-things.jpg">
                    <div>
                        <div id="moveContainer"> <!-- signSec_pageTwo -->
                            <section id="signInSec" class="signSec_page"> <!-- verified -->
                                <span id="helpSpan" class="signSec_btn">?</span>
                                <span id="signUpSecSpan" class="signSec_btn">Sign Up ▶</span>
                                <div id="logo"></div>
                                <div>
                                    <input id="signInput_email" type="email" placeholder="example@example.com" size="25" autofocus>
                                    <!--<div id="autoCompleteDiv">
                                        <div></div>
                                    </div>-->
                                    <input id="signInput_passwd" type="password" placeholder="Password" size="25">
                                </div>
                                <div>
                                    <span id="signInSpan" class="signSec_btn">Sign In :</span>
                                    <span id="signModeSpan" class="signSec_btn">Sign Mode</span>
                                </div>
                            </section>
                            <section id="signUpSec" class="signSec_page">
                                <input id="signUpInput_email" type="email" name="email" placeholder="example@example.com" size="25" required>
                                <input id="signUpInput_password" type="password" name="password" placeholder="Password" size="25" required>
                                <input id="signUpInput_confirmPassword" type="password" placeholder="Confirm Password" size="25" required>
                                <span id="signUpSpan" class="signSec_btn">Sign Up</span>
                            </section>
                        </div>
                    </div>
                </div>
            </section>
<?php
    }
}