<?php
/**
 * Created by IntelliJ IDEA.
 * User: Osirp
 * Date: 2017-01-20
 * Time: 오후 6:20
 */

namespace oMusic\application\Controllers;



use oMusic\application\Models\Authorization;
use oMusic\application\Models\Playlist;
use oMusic\application\Models\Socket;
use oMusic\application\Models\State;
use oMusic\application\Models\Upload;
use oMusic\application\Views\Board;

class Controller
{
    static public $state, $playlist;
    function __construct()
    {
        session_start();
        self::$state = new State();
        self::$state->run();
        self::$playlist = new Playlist();
        self::$playlist->run();
        if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') // ajax 요청
        {
            header("Content-Type:application/json");
            if (isset($_SERVER['HTTP_X_REQUESTED_PERMISSION'])) // 권한 요청
            {
                $authorization = new Authorization();
                if(!$authorization->run())
                {
                    self::$state->msg('인증 에러. 관리자에게 문의해 주시기 바랍니다.');
                }
                // 인증에 따라 플레이 리스트 재설정
                self::$playlist->run();
            }
            else if (isset($_SERVER['HTTP_X_REQUESTED_FILEUPLOAD']) && strtolower($_SERVER['HTTP_X_REQUESTED_FILEUPLOAD']) == 'upload')
            {
                $upload = new Upload();
                if (!$upload->run())
                {
                    self::$state->msg('업로드 에러. 관리자에게 문의해 주시기 바랍니다.');
                }
            }
            else if (isset($_SERVER['HTTP_X_SEND_SOCKETID']) && isset($_POST['socketId']))
            {

                self::$state->setSocketId();
            }
            else
            {
                $socket = new Socket();
                $socket->sendMsg('하나둘셋');
                $socket->sendMsg('서버에서 보내는 메세지');

            }
            self::$state->setPlaylist(self::$playlist->getPlaylist());
            echo json_encode(self::$state);
        }
        else
        {

            $Board = new Board();

        }

    }
}