<?php
/**
 * Created by IntelliJ IDEA.
 * User: Osirp
 * Date: 2017-01-21
 * Time: 오후 5:08
 */

namespace oMusic;

// 참고 - https://developer.mozilla.org/ko/docs/WebSockets/Writing_WebSocket_servers
class Wsocket
{
    private $master;
    private $sockets = array();
    private $users = array();
    private $debug = true;
    private $null = null;

    //private $msgStatus = null;

    function __construct($address, $port)
    {
        /*$this->msgStatus = new \stdClass();
        $this->msgStatus->isFinished = true;
        $this->msgStatus->maskingKey = '';
        $this->msgStatus->totalDataByte = 0;
        $this->msgStatus->ProcessedDataByte = 0;
        $this->msgStatus->unmaskedData = [];*/

        ini_set('memory_limit','512M');
        set_time_limit(0);
        ob_implicit_flush(true);

        $this->master = socket_create(AF_INET, SOCK_STREAM, SOL_TCP)  or die('socket_create() failed');
        socket_bind($this->master, $address, $port)                   or die('socket_bind() failed');
        socket_listen($this->master)                                  or die('socket_listen() failed');

        $this->say('Server Started : ' . date('Y-m-d H:i:s'));
        $this->say('Listening on   : ' . $address . " port " . $port);
        $this->say('Master socket  : ' . $this->master . "\n");

        $this->sockets[] = $this->master; // 모든 소켓을 한번에 관리하기 위해
        while (true)
        {
            $changed = $this->sockets; // 왜 이러는지 모르지만 이렇게 안하면 싱글통신만 가능
            socket_select($changed, $this->null, $this->null, null) or die('socket_select() fail');
            foreach ($changed as $socket)
            {
                if ($socket == $this->master) // 서버 소켓에 이벤트 발생
                {
                    $this->log('서버 소켓에 이벤트 발생');
                    $uSocket = socket_accept($this->master);
                    if(!($uSocket < 0))
                    {
                        $this->addUser($uSocket);
                    }
                    else
                    {
                        $this->log('socket_accept() failed');
                    }
                }
                else // 클라이언트 소켓에 이벤트 발생
                {
                    $this->log('클라이언트 소켓에 이벤트 발생');
                    $bytes = @socket_recv($socket, $buffer, 2048, 0);
                    if ($bytes != false || $bytes != 0)
                    { // 클라이언트로부터 응답이 있으면
                        $user = $this->getUser($socket);
                        if (!$user->from) $this->fromWhere($user, $buffer); // php 인지 web 인지
                        if ($user->from == 'php') // php 에서 온 응답
                        {
                            // 연속으로 받을 경우를 대비
                            $recvData = explode('&&&&', $buffer);
                            foreach ($recvData as $i)
                            {
                                if (!empty($i))
                                {
                                    $data = json_decode($i);
                                    // 포함되어있는 브라우저 소켓 ID에 메세지 전달
                                    $clientUser = $this->getUserById($data->socketId);
                                    $this->responseMsg($clientUser, $data->msg);
                                }
                            }

                        }
                        else // web 에서 온 응답
                        {
                            if (!$user->handshake)
                            { // 악수 안했으면
                                if (!$this->doHandshake($user, $buffer))
                                { // 악수 실패하면
                                    $this->log('handshake failed');
                                }
                                else
                                {
                                    if (!$this->responseMsg($user, 'Success Handshake')) $this->disconnect($socket);
                                }
                            }
                            else
                            { // 악수 했으면
                                // 응답 메세지 보내기
                                // 마스킹 풀기
                                $strData = $this->unmasking($user, $buffer);
                                /*foreach ($this->users as $fuser) { // 채팅기능
                                    if($fuser != $user)
                                        if ($fuser->msgStatus->isFinished && !$this->responseMsg($fuser, $strData)) $this->disconnect($socket);
                                }*/
                                if ($user->msgStatus->isFinished && !$this->responseMsg($user, $strData)) $this->disconnect($socket);

                            }
                        }

                    }
                    else
                    { // 클라이언트로부터 응답이 없으면
                        $this->disconnect($socket);
                    }
                }
            }
        }

    }

    private function addUser($socket)
    {
        $user = new SocketUser();
        $this->log('할당된 유니크 ID : ' . $user->id);
        $user->socket = $socket;
        array_push($this->users, $user);
        array_push($this->sockets, $socket);
        $this->say($socket . ' CONNECTED');
        $this->say(date('d/n/Y ') . 'at ' . date('H:i:s T'));
    }

    private function getUser($socket)
    {
        $found = null;
        foreach ($this->users as $user)
        {
            if ($user->socket == $socket)
            {
                $found = $user;
                break;
            }
        }
        return $found;
    }
    private function getUserById($id)
    {
        $found = null;
        foreach ($this->users as $user)
        {
            if ($user->id == $id)
            {
                $found = $user;
                break;
            }
        }
        return $found;
    }

    private function fromWhere($user, $buffer)
    {
        if (preg_match("/\"request_from\":\"(php)\"/", $buffer, $match))
        {
            $user->from = strtolower($match[1]);
            $this->log($user->from);
        }
        else
        {
            $user->from = 'web';
            $this->log($user->from);
        }
    }

    private function doHandshake($user, $buffer)
    {
        $this->say('Requesting handshake..');
        $this->log($buffer);
        if (preg_match("/Sec-WebSocket-Key: (.*)\r\n/", $buffer, $match))
        {
            $key = base64_encode(SHA1($match[1]."258EAFA5-E914-47DA-95CA-C5AB0DC85B11", true));
        }
        else
        {
            $this->log('Error : Missing Sec-WebSocket-Key');
            return false;
        }
        // 응답헤더 작성
        $response_header = "HTTP/1.1 101 Switching Protocols\r\n" .
            "Upgrade: WebSocket\r\n" .
            "Connection: Upgrade\r\n" .
            "Sec-WebSocket-Accept: " . $key . "\r\n\r\n";
        // 악수 손내밀기
        $this->log($response_header);
        $this->log('Sec-WebSocket-Accept: ' . $key);
        if (socket_write($user->socket, $response_header) === false)
        {
            $this->log('socket_write() failed');
            return false;
        }
        $user->handshake = true;
        $this->say('Done Handshake');
        return true;
    }

    private function unmasking($user, $buffer)
    {
        $bufferData_byte = strlen ($buffer);
        $data = $this->str2bin($buffer);

        $masking_key = [];
        $masked_data = [];
        $unmasked_data = [];

        if ($user->msgStatus->isFinished)
        {
            $this->log('총 받은 데이터 : ' . $bufferData_byte);

            $opcode = bindec(substr($data[0], 4));
            if ($opcode == 1) // UTF-8 string type
            {
                $this->log('Data Type : UTF-8 string');
            }
            else if ($opcode == 2) // blob type
            {
                $this->log('Data Type : blob');
            }

            // 데이터 길이 판별
            $payloadLenField_len = bindec($data[1]);
            $payloadLenField_len = ($payloadLenField_len >= 128) ? $payloadLenField_len ^ 128 : $payloadLenField_len; // 첫번째 비트 0으로 (마스킹 해제)
            $data[1] = str_pad(decbin($payloadLenField_len), 8, '0', STR_PAD_LEFT); // 이진 문자열로 변환

            // 데이터 길이에 따른 마스킹 필드 인덱스 찾기
            if($payloadLenField_len == 127) // 추가 확장 payload필드 사용
            {
                $this->log('추가 확장 payload 필드' );
                $maskingField_start = 10; $maskingField_end = 14;
                $data_byte = bindec(implode(array_slice($data, 2, $maskingField_start - 2)));
            }
            else if ($payloadLenField_len == 126)// 확장 payload필드 사용
            {

                $this->log('확장 payload 필드');
                $maskingField_start = 4; $maskingField_end = 8;
                $data_byte = bindec(implode(array_slice($data, 2, $maskingField_start - 2)));
            }
            else // 기본 payload필드 사용 데이터길이 125 이하
            {
                $this->log('기본 payload 필드');
                $maskingField_start = 2; $maskingField_end = 6;
                $data_byte = bindec(implode(array_slice($data, 1, $maskingField_start - 1)));
            }


            $this->log('데이터 길이 : ' . $data_byte);

            // 마스킹 해제 준비작업
            for ($i = $maskingField_start; $i < $maskingField_end; $i++) // 마스킹 키 위치
            {
                $masking_key = array_merge($masking_key, str_split($data[$i]));
            }
            $data_len = count($data);
            for ($i = $maskingField_end; $i < $data_len; $i++) // 문자열 데이터 위치
            {
                $masked_data = array_merge($masked_data, str_split($data[$i]));
            }


            // 마스킹 해제
            $masked_data_len = count($masked_data);
            for ($i = 0; $i < $masked_data_len; $i++)
            {
                $unmasked_data[] = intval($masked_data[$i]) ^ intval($masking_key[$i % 32]);
            }
            //$this->log('받은 메세지' . $this->bin2str($unmasked_data));

            //$result = array_slice($data,0,$maskingField_start); // 마스킹 키 부분 제거
            //$result = array_merge($result, $unmasked_data); // 데이터 병합

            // 잘린 데이터이면 마스킹 키 저장
            if ($bufferData_byte < $data_byte + $maskingField_end) // 잘림
            {
                $user->msgStatus->isFinished = false;
                $user->msgStatus->maskingKey = $masking_key;
                $user->msgStatus->totalDataByte = $data_byte + $maskingField_end;
                $user->msgStatus->ProcessedDataByte = $bufferData_byte;
                $user->msgStatus->unmaskedData = $unmasked_data;
                return null;
            }
            // 안잘림
            $user->magStatusReset();
            $this->log('언 마스킹 성공');
            return $this->bin2str($unmasked_data); // 문자열로 리턴
        }
        else /* 총 길이가 2048 넘어가면 잘림 기능봉인 */
        {
            $this->log('잘린 데이터');
            $this->log('총 받은 데이터 : ' . $bufferData_byte);
            $unmasked_data = $user->msgStatus->unmaskedData;

            $masking_key = $user->msgStatus->maskingKey;

            $data_len = count($data);
            // 마스킹 해제 준비작업
            for ($i = 0; $i < $data_len; $i++) // 문자열 데이터 위치
            {
                $masked_data = array_merge($masked_data, str_split($data[$i]));
            }

            // 마스킹 해제
            $masked_data_len = count($masked_data);
            for ($i = 0; $i < $masked_data_len; $i++)
            {
                $unmasked_data[] = intval($masked_data[$i]) ^ intval($masking_key[$i % 32]);
            }
            $unmasked_data = array_merge($unmasked_data, $unmasked_data); // 데이터 병합

            $user->msgStatus->ProcessedDataByte += $bufferData_byte;
            // 또 잘린 데이터이면 데이터 저장
            if ($user->msgStatus->ProcessedDataByte < $user->msgStatus->totalDataByte) // 잘림
            {
                $user->msgStatus->isFinished = false;
                $user->msgStatus->unmaskedData = $unmasked_data;
                return null;
            }
            // 안잘림
            $user->magStatusReset();
            $this->log('언 마스킹 성공');
            return $this->bin2str($unmasked_data); // 문자열로 리턴
        }
    }

    private function responseMsg($user, $msg)
    {

        $strObj = new \stdClass();
        $strObj->text = $msg;
        $strObj->socketId = $user->id;

        $msg = json_encode($strObj);

        $msg = $this->putMsgHeader($msg);
        if (socket_write($user->socket, $msg) === false)
        {
            $this->log('responseMsg() failed');
            return false;
        }
        $this->log('메세지 전송 성공 ------');
        //$this->say('Sent Message: '. $msg);
        return true;
    }

    private function putMsgHeader($msg)
    {
        $header = ['10000001'];
        $msg_byte = strlen($msg);
        if ($msg_byte <= 125)
        {
            $header[] = str_pad(decbin($msg_byte), 8, '0', STR_PAD_LEFT);
        }
        else if($msg_byte <= 65535)
        {
            $header[] = str_pad(decbin(126), 8, '0', STR_PAD_LEFT);
            $header[] = str_pad(decbin($msg_byte), 16, '0', STR_PAD_LEFT);
        }
        else if($msg_byte <= pow(2, 64))
        {
            $header[] = str_pad(decbin(127), 8, '0', STR_PAD_LEFT);
            $header[] = str_pad(decbin($msg_byte), 64, '0', STR_PAD_LEFT);
        }
        return $this->bin2str($header) . $msg;
    }


    private function disconnect($socket)
    {
        $found = null;
        $n = count($this->users);
        for ($i = 0; $i < $n; $i++){
            if ($this->users[$i]->socket == $socket)
            {
                $found = $i;
                break;
            }
        }
        if (!is_null($found))
        { // 유저 삭제
            array_splice($this->users, $found, 1);
        }

        $index = array_search($socket, $this->sockets);
        $this->say($socket . ' DISCONNECTED');

        socket_close($socket);
        if ($index >= 0)
        { // 소켓 배열에서 삭제
            array_splice($this->sockets, $index, 1);
        }
    }

    private function say($msg)
    {
        echo $msg . "\n";
    }
    private function log($msg)
    {
        if ($this->debug)
        {
            echo $msg . "\n";
        }
    }

    private function str2bin($input) {
        $result = [];
        $arr = str_split($input);
        foreach ($arr as $i)
        {
            $result[] = str_pad(decbin(ord($i)), 8, '0', STR_PAD_LEFT);
        }
        return $result;
    }
    private function bin2str($input) {
        if (gettype($input) == 'array') $input = implode($input);
        $result = '';
        $arr = str_split($input, 8);
        foreach ($arr as $i)
        {
            $result .= chr(bindec($i));
        }
        return $result;
    }

}
class SocketUser
{
    public $id, $socket, $handshake, $from, $msgStatus;
    function __construct()
    {
        $this->id = uniqid();
        $this->from = false;
        $this->handshake = false;
        $this->msgStatus = new \stdClass();
        $this->msgStatus->unmaskedData = [];
        $this->magStatusReset();
    }
    function magStatusReset()
    {
        $this->msgStatus->isFinished = true;
        $this->msgStatus->maskingKey = '';
        $this->msgStatus->totalDataByte = 0;
        $this->msgStatus->ProcessedDataByte = 0;
        unset($this->msgStatus->unmaskedData);
    }
}

new Wsocket(getHostByName(getHostName()), 8100); //fixme 웹소켓 접속할 PORT